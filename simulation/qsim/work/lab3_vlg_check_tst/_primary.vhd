library verilog;
use verilog.vl_types.all;
entity lab3_vlg_check_tst is
    port(
        SEG_0           : in     vl_logic;
        SEG_1           : in     vl_logic;
        SEG_2           : in     vl_logic;
        SEG_3           : in     vl_logic;
        SEG_4           : in     vl_logic;
        SEG_5           : in     vl_logic;
        SEG_6           : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end lab3_vlg_check_tst;
