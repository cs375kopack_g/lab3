library verilog;
use verilog.vl_types.all;
entity lab3 is
    port(
        SEG_0           : out    vl_logic;
        a               : in     vl_logic;
        b               : in     vl_logic;
        c               : in     vl_logic;
        d               : in     vl_logic;
        SEG_1           : out    vl_logic;
        SEG_2           : out    vl_logic;
        SEG_3           : out    vl_logic;
        SEG_4           : out    vl_logic;
        SEG_5           : out    vl_logic;
        SEG_6           : out    vl_logic
    );
end lab3;
